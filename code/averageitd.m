function itdaverage = averageitd(corrT)
% menghitung rata2 hasil crosscorrelation
% untuk memperoleh ITD rata2

[nfilter,ndelay] = size(corrT);
integratedccfunctionT = zeros(1,ndelay);

for delay = 1:ndelay
    integratedccfunctionT(delay) = 0;
    for filter = 1:nfilter
        integratedccfunctionT(delay) = integratedccfunctionT(delay) + corrT(filter, delay);
    end
end

itdaverage = integratedccfunctionT;