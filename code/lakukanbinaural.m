function [itd, ild, campur_kiri, kiri] = lakukanbinaural(suara_target, suara_masker, azimuth_target, azimuth_masker, pow, fs)
    % melakukan pencampuran secara binaural dengan frekuensi sampling
    % dari sumber suara target dan masker adalah 16000 Hz.
    % 
    % suara target : sinyal suara target
    % suara masker : sinyal suara masker
    % azimuth_target : nilai azimuth dari sumber suara target
    % azimuth masker : nilai azimuth dari sumber suara masker
    % pow : selisih power antara sumber suara target terhadap sumber suara masker (dB)
    % fs : frekuensi sampling suara

    %% load data hrtf
    fHRTF = '../data/hrtf/';
    % hrtf_right_t      = [fHRTF 'R' int2str(azimuth_target)];
    % hrtf_target_right = load(hrtf_right_t);
    hrtf_left_t       = [fHRTF 'L' int2str(azimuth_target)];
    hrtf_target_left  = load(hrtf_left_t);
    hrtf_right_m      = [fHRTF 'R' int2str(azimuth_masker)];
    hrtf_masker_right = load(hrtf_right_m);
    hrtf_left_m       = [fHRTF 'L' int2str(azimuth_masker)];
    hrtf_masker_left  = load(hrtf_left_m);

    %% load data suara
    target = suara_target;
    masker = suara_masker;

    %% data spasial
    target = pow2(target, 15);
    masker = pow2(masker, 15);

    target = target * 10 ^ (pow / 20);

    if length(target) > length(masker)
        target = target(1 : length(masker), :);
    elseif length(target) < length(masker)
        masker = masker(1 : length(target), :);
    end

    TL = conv(target, hrtf_target_left);
    % TR = conv(target, hrtf_target_right);
    ML = conv(masker, hrtf_masker_left);
    MR = conv(masker, hrtf_masker_right);
    MIXL = TL + ML;
    % MIXR = TR + MR;

    %% gammatone filterbank
    % gfTL = gammatone(TL,128,[80,5000],fs);
    % gfTR = gammatone(TR,128,[80,5000],fs);
    gfML = gammatone(ML,128,[80,5000],fs);
    gfMR = gammatone(MR,128,[80,5000],fs);
    % gfMIXL = gammatone(MIXL,128,[80,5000],fs);
    % gfMIXR = gammatone(MIXR,128,[80,5000],fs);

    %% hair cell filter
    % hcTL = meddis(gfTL,fs);
    % hcTR = meddis(gfTR,fs);
    hcML = meddis(gfML,fs);
    hcMR = meddis(gfMR,fs);
    % hcMIXL = meddis(gfMIXL,fs);
    % hcMIXR = meddis(gfMIXR,fs);

    %% cochleagram
    % cgTL = cochleagram(hcTL,320);
    % cgTR = cochleagram(hcTR,320);
    % cgML = cochleagram(hcML,320);
    % cgMR = cochleagram(hcMR,320);
    % cgMIXL = cochleagram(hcMIXL,320);
    % cgMIXR = cochleagram(hcMIXR,320);

    %% ekstrak binaural cue
    % ITD
    % itdchanframeT = itdchanframe(hcTL, hcTR, fs);
    itdchanframeM = itdchanframe(hcML, hcMR, fs);
    % itdaverageMIX = averageitd(corrMIX); % corrMIX ada di kumpulkandata.m

    % ild
    % ildchanframeT = ildchanframe(hcTL, hcTR, fs);
    ildchanframeM = ildchanframe(hcML, hcMR, fs);
    % ildchanframeMIX = ildchanframe(hcMIXL, hcMIXR);

    %% relative strength

    % ratioL = ratiochanframe(hcTL, hcML);
    % ratioR = ratiochanframe(hcTR, hcMR);

    %% tentukan output
    campur_kiri = MIXL;
    % campur_kanan = MIXR;
    itd = caripeakitd( itdchanframeM );
    ild = ildchanframeM;
    % ratio = ratioL;
    kiri = TL;

end