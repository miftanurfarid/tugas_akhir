function loud = loudness(freq)
% Compute loudness level in Phons on the basis of equal-loudness functions.
% It accounts a middle ear effect and is used for frequency-dependent gain adjustments.
% This function uses linear interpolation of a lookup table to compute the loudness level, 
% in phons, of a pure tone of frequency freq using the reference curve for sound 
% pressure level dB. The equation is taken from section 4 of BS3383.
% 
% Written by ZZ Jin, and adapted by DLW in Jan'07

dB = 60;

elq = load('f_af_bf_cf.mat');
% Stores parameters of equal-loudness functions from BS3383,"Normal equal-loudness level
% contours for pure tones under free-field listening conditions", table 1.
% f (or ff) is the tone frequency, af and bf are frequency-dependent coefficients, and
% tf is the threshold sound pressure level of the tone, in dBs   

if (freq < 20 || freq > 12500)
    return;
end

i = 1;

while(elq.ff(i) < freq)
    i = i + 1;
end
afy  = elq.af(i-1)+(freq-elq.ff(i-1))*(elq.af(i)-elq.af(i-1))/(elq.ff(i)-elq.ff(i-1));
bfy  = elq.bf(i-1)+(freq-elq.ff(i-1))*(elq.bf(i)-elq.bf(i-1))/(elq.ff(i)-elq.ff(i-1));
cfy  = elq.cf(i-1)+(freq-elq.ff(i-1))*(elq.cf(i)-elq.cf(i-1))/(elq.ff(i)-elq.ff(i-1));
loud = 4.2+afy*(dB-cfy)/(1+bfy*(dB-cfy));