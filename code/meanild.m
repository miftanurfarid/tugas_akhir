function [ meanild ] = meanild(hcL,hcR)
%   Menghitung rata-rata nilai ild
%   meanild : rata-rata nilai ILD pada setiap kanal frekuensi
%   hcL     : hair cell telinga kiri
%   hcR     : hair cell telinga kanan

[numChan,~] = size(hcL);

a = zeros(numChan,1);
b = zeros(numChan,1);
for i = 1:numChan
    a(i,:) = hcR(i,:) * hcR(i,:)';
    b(i,:) = hcL(i,:) * hcL(i,:)';
    c = 20 * log10(a ./ b);
end

meanild = c;

end

