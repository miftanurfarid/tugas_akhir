function result = myLoadFile(fileName, environmentName)
    % load data tanpa class struct
    tmp = load(fileName, environmentName);
    result = tmp.(environmentName);
end