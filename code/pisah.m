function pisah
    % pisah.m - pisah data suara target dari masker berdasarkan data training
    % yang sudah ada.
    tic
    azimuth_target = 0;     % azimuth sumber suara target
    azimuth_masker = 30;    % azimuth sumber suara masker
    pow = 10;               % beda power antara suara target terhadap suara masker (dB)
    gender_target = 'mmht'; % gender target. mmht = male
    gender_masker = 'fena'; % gender target. mmht = fena
    sr = 16000;              % sampling rate
    
    %% Proses Data Training
    [ITD,ILD,RATIO] = kumpulkandata(azimuth_target,azimuth_masker,pow);
    
    %% Mapping
    dire = '/';
    dir_map = sprintf('../data/t%dm%dpow%d%s', azimuth_target, azimuth_masker, pow, dire);
    mkdir(dir_map);
    mapping_name = sprintf('%smappingt%dm%dpow%d', dir_map, azimuth_target, azimuth_masker, pow);
    decisionrules(ITD, ILD, RATIO, mapping_name);
    
    %% Pisah data suara
    fprintf('Proses data TA Fafa\nPemisahan Data Suara...');
    indeks_nomer_suara = myLoadFile('nomer_kalimat.mat', 'nomer_kalimat');
    D = myLoadFile(mapping_name, 'D');
    Region = myLoadFile(mapping_name, 'Region');

    for n = 1:length(indeks_nomer_suara)
        nomer_suara = indeks_nomer_suara(n);
        fprintf('%02d%%', floor((nomer_suara - 320) / (500 - 320) * 100));
        s1 = sprintf('../data/wav%s%s%s%s_%04d.wav',dire,gender_target,dire,gender_target,nomer_suara);
        [target, fs] = audioread(s1);
        target = resample(target, sr, fs);
        nomer_masker = 320;
        s2 = sprintf('../data/wav%s%s%s%s_%04d.wav',dire,gender_masker,dire,gender_masker,nomer_masker);
        [masker, fs] = audioread(s2);
        masker = resample(masker, sr, fs);
        [itd, ild, campur_kiri, kiri] = lakukanbinaural(target, masker, azimuth_target, azimuth_masker, pow, sr);
        ebmask = makeMask(itd, ild, Region, D);
        resynth = synthesis(campur_kiri, ebmask);
        nama_resynth = sprintf('%s%s_%04d_resynth.wav', dir_map, gender_target, nomer_suara);
        nama_asli = sprintf('%s%s_%04d_ori.wav', dir_map, gender_target, nomer_suara);
        resynth = resynth ./ (max(abs(resynth * 2)));
        kiri = kiri ./ (max(abs(kiri * 2)));
        audiowrite(nama_resynth, resynth, sr);
        audiowrite(nama_asli, kiri, sr);
        pval = pesq(nama_asli,nama_resynth);
        nama_pval = sprintf('%s%s_%04d_pesq_val.txt',dir_map,gender_target,nomer_suara);
        fileID = fopen(nama_pval,'w');
        fprintf(fileID,'%f \n',pval);
        fclose(fileID);
        fprintf('\b\b\b');
    end

    fprintf('\bSelesai!\n');
    toc

    end
    