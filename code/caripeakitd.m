function [ out ] = caripeakitd( itdchanframeM )
% mencari nilai itd dari posisi peak ITD hasil proses itdchanframe
% 
% itdchanframeM : keluaran dari itdchanframe
% out : nilai ITD

[row, col, ~] = size(itdchanframeM);
ITDtempm = zeros(row,col);

for i = 1:row
    for j = 1:col
        [~, b] = max(itdchanframeM(i,j,:));
        ITDtempm(i,j) = b;
    end
end

out = ITDtempm;

end